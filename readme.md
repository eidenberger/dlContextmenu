dlContextMenu
=============

dlContextMenu gives you the ability to quickly install a simple css and javascript based dropdown menu.

Features
--------

* Transform a simple select element in a stylish menu.
* Load data via Json objects.
* Css3 enabled.
* Fully customisable menu elements.
* Customise sizes.
* Custom onClick events.
* Autohide option, on mouseout event.

Todo
----
* Add up/down key navigation.
* Add Alphanum. key shortcuts.

Requirements
------------

MooTools core 1.3