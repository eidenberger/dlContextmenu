var dlContextMenu = new Class({
    Implements: [Options, Events],
    options: {
		'container_class': 'dlmenucontainer',
		'autohide': false,
		'key': '',
		'value': '',
		'size': {
			'height': 200,
			'width': 180
		},
		'offset': {
			'x': 0,
			'y': 0			
		},
		'scroll_enabled': true,
		'key_select': true,
		'data' : false,
		'default_caption' : 'click here'
		
	},
    initialize: function(el, options) {
        this.setOptions(options);
		this.element = el;
		
		// Select element case
		if (this.element.get('tag') == 'select') {
			this.processSelectData();
			this.processSelectElement();
		};
		
		// console.log(this.options.data)
		
		this.element.addEvent('click', function(e) {
			e.stop();
			
			this.initMask();
			this.showMenu();
		}.bind(this));
    },
	processSelectData : function() {
		
		this.options.data = new Array();
		
		// Let's collect the select data...
		this.element.getChildren('option').each(function(item, index) {
			this.options.data.push({key : item.get('value'), value : item.get('html') });
		}.bind(this));
		
		// Set default keys and values
		this.options.key = 'key';
		this.options.value = 'value';		
	},
	processSelectElement : function() {
		// console.log(this.element)
		var action_element = new Element('a', {
			'href': '#',
			'html': this.options.default_caption,
			'class': 'dl_menu_action'
		}).inject(this.element,'after');
		this.element.destroy();
		console.log(action_element)
		this.element = action_element;
	},
	initMask: function() {		
		if (this.mask) {
			this.mask.setStyle('display', 'block');
		} else {
			this.mask = new Element('div', {
				'class' : 'dlMask',
				'styles' : {
					'height' : document.body.getScrollSize().y,
					'width' : document.body.getScrollSize().x,
					'position' : 'absolute',
					'top' : 0,
					'left' : 0
				},
				'events' :{
					'click' : function(e) {
						this.closeMenu();
					}.bind(this)
				} 
			}).inject(document.body);
			
			
		};
		
	
	},
	showMenu: function() {
		
		this.menu_container = new Element('div', {
			'html': 'some content',
			'class': this.options.container_class,
			'styles': {
				'position': 'absolute',
				'top': this.element.getPosition().y + this.element.getSize().y + this.options.offset.y,
				'left': this.element.getPosition().x + this.options.offset.x,
				'width': this.options.size.width,
				'height': this.options.size.height
			},
			'events': {
				'mousewheel': function(e) {
					e.stop();
					
					if (Browser.safari || Browser.chrome) {
						if (e.event.wheelDeltaX == '0') {
							this.scroll(e);
						};
					} else if(Browser.firefox) {
						if (e.event.axis == '2') {
							this.scroll(e);
						};
					}
				}.bind(this)
			}
		}).inject(document.body);
		
		// Autohide option...
		if (this.options.autohide) {
			this.menu_container.addEvent('mouseleave', function(e) {
				this.closeMenu();
			}.bind(this))
		};
		
		
		if (this.options.key_select) {
			document.window.addEvent('keydown', function(e) {
				
				if (e.key == 'down') {
				    e.stop();
					console.log('go down')
				} else if (e.key == 'up') {
				    e.stop();
					console.log('go up')
				};
			});
		};
		
		this.populateMenu();		
		
		// Set sizes, mins, maxs & blockers for scroll
		this.min_y = this.menu_container_ul.getStyle('top').toInt();
		this.block_up = true;
		this.block_down = false;
		this.max_y = - this.menu_container_ul.getScrollSize().y.toInt() + this.options.size.height + this.min_y;


		this.scroll_handle_height = (-this.max_y / this.options.size.height).round();
		// Build scroll handle
		this.scrollhandle = new Element('div', {
			'class' : 'dl_scrollhandle',
			'styles' : {
				'width' : '6px',
				'height' : this.scroll_handle_height,
				'position' : 'absolute',
				'top' : 2,
				'left' : 1
			}
		}).inject(this.menu_container);
		
		// Fire show event
		this.show();
		

	},
	scroll: function(e) {
		if (this.options.scroll_enabled) {
			if (Browser.chrome || Browser.safari) {
				var scroll_value = (e.wheel * 15).round();
			} else {
				var scroll_value = e.wheel.round() * 5;
			};

			if (this.repos_down === true) {
				this.menu_container_ul.setStyle('top', this.max_y);
				this.scrollhandle.setStyle('top', (-this.max_y)/this.scroll_handle_height.round());
				this.repos_down = false;
			};

			if (this.repos_up === true) {
				this.menu_container_ul.setStyle('top', this.min_y);
				this.scrollhandle.setStyle('top', 2);
				this.repos_up = false;
			};

			if (scroll_value > 0) {
				if (this.block_up === false) {

					if (this.menu_container_ul.getStyle('top').toInt() > this.min_y) {
						this.block_down = false;
						this.block_up = true;
						this.repos_up = true;
					}

					var new_scroll_value = this.menu_container_ul.getStyle('top').toInt() + scroll_value;

					this.scrollhandle.setStyle('top', (-new_scroll_value)/this.scroll_handle_height.round());
					this.menu_container_ul.setStyle('top', new_scroll_value);

					this.block_down = false;
				};

			} else if (scroll_value <= 0) {
				// Scroll down...
				if (this.block_down === false) {

					if (this.menu_container_ul.getStyle('top').toInt() < this.max_y) {

						this.block_down = true;
						this.block_up = false;
						this.repos_down = true;
					};
					var new_scroll_value = this.menu_container_ul.getStyle('top').toInt() + scroll_value;

					this.scrollhandle.setStyle('top', (-new_scroll_value)/this.scroll_handle_height.round())
					this.menu_container_ul.setStyle('top', new_scroll_value);

					this.block_up = false;
				};

			};	
		};
	},
	populateMenu: function() {
		this.menu_container.empty();
		
		if (this.options.data.length > 0) {
			
			this.menu_container_ul = new Element('ul', {
				'class': 'dl_menu_container_ul',
				'styles' : {
					'position' : 'relative',
					'top' : '0'
					//'height': this.options.size.height
				}
			}).inject(this.menu_container)
			
			this.options.data.each(function(item, index) {
			
				new Element('li',{
					'class': 'dl_menu_element', 
					'html': item[this.options.value],
					'events': {
						'click': function(e) {
							this.select(item[this.options.key], item[this.options.value]);
						}.bind(this)
					}
				}).inject(this.menu_container_ul);

			}.bind(this))

		} else {
			// console.log('else')
		};
		
		// Auto Resize menu height if not enough data & disable scroll
		if (this.menu_container_ul.getStyle('height').toInt() < this.options.size.height.toInt()) {
			this.menu_container.setStyle('height', 'auto');
			this.options.scroll_enabled = false;
		};		
	},
    show: function() {
        this.fireEvent('show');
    },
	select: function(key, value) {
		this.fireEvent('select',[key, value]);
		this.closeMenu();
	},
	closeMenu: function() {
		this.mask.setStyle('display', 'none');
		this.menu_container.destroy();
	}
 
});